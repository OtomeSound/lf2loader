#include "Utils.h"
#include <cstdarg>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <algorithm>

#include <strsafe.h>
#include <shellapi.h>

// given directory where data.txt is, and load its version and subver
bool Utils::loadVersion(std::wstring path)
{
	std::ifstream fin;
	std::string line1, line2;
	/*
	version format in data\\data.txt

	# version: 5
	# subver : A
	...
	*/
	try {
		fin.open(path + L"\\data.txt", std::ifstream::in);
		std::getline(fin, line1);
		std::getline(fin, line2);
		fin.close();

		this->version = line1[11];
		this->subver = line2[10];
	} catch (...) {
		return false;
	}
	
	return true;
}

// for a second level folders, search for available versions
bool Utils::loadAvailable()
{
	/*
	Directory Structure of "版本選擇"
	版本選擇
		1.0
			A版\data\data.txt
			...
		2.0
			A版\data\data.txt
			...
		...
	*/
	int numOfFirstDirectories = 0;
	std::wstring baseDir = L"版本選擇";
	std::wstring path;
	WIN32_FIND_DATA ffd;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	std::vector<std::wstring> directories;

	try {
		// append '\*' for windcard search
		baseDir += L"\\*";

		// List all first directory.
		hFind = FindFirstFile(baseDir.c_str(), &ffd);
		if (hFind == INVALID_HANDLE_VALUE) {
			FindClose(hFind);
			throw("No files in Folder");
		}
		do {
			if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
				if (ffd.cFileName[0] == '.') continue;
				path = baseDir + ffd.cFileName;
				path.erase(baseDir.size() - 1, 1);
				directories.push_back(path);
			}
		} while (FindNextFile(hFind, &ffd) != 0);

		numOfFirstDirectories = directories.size();

		// List all second directory.
		for (int i = 0; i < numOfFirstDirectories; i++) {
			baseDir = directories[0];
			directories.erase(directories.begin());

			// append '\*' for windcard search
			baseDir += L"\\*";

			hFind = FindFirstFile(baseDir.c_str(), &ffd);
			if (hFind == INVALID_HANDLE_VALUE) {
				FindClose(hFind);
				throw("No files in Folder");
			}
			do {
				if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
					if (ffd.cFileName[0] == '.') continue;
					path = baseDir + ffd.cFileName;
					path.erase(baseDir.size() - 1, 1);
					directories.push_back(path);
				}
			} while (FindNextFile(hFind, &ffd) != 0);
		}

		// check data.txt file is exist or not
		for (auto it = directories.begin(); it != directories.end(); ++it) {
			// check data.txt is exist or not
			std::ifstream fin(*it + L"\\data\\data.txt");
			if (!fin.good()) directories.erase(it);

			// import this version as available
			struct availableVersion thisVersion;
			thisVersion.strRealPath = *it + L"\\data";
			size_t pos1 = (*it).find(L"\\");
			size_t pos2 = (*it).find(L"\\", pos1 + 1);
			thisVersion.strVersion = (*it).substr(pos1 + 1, pos2 - pos1 - 1);
			thisVersion.strSubver = (*it).substr(pos2 + 1);

			// get version, subver for reverse mapping
			std::string line1, line2;
			std::getline(fin, line1);
			std::getline(fin, line2);
			fin.close();

			thisVersion.version = line1[11];
			thisVersion.subver = line2[10];

			versions.push_back(thisVersion);
		}

	} catch (...) {
		return false;
	}

	return true;
}

// draw listbox and buttons, called by WM_CREATE
void Utils::DrawGUIInterface(HWND hWnd)
{
	// 43, 50: create a list box
	hwndListbox = CreateWindow(
		L"LISTBOX",
		NULL,
		WS_CHILD | WS_VISIBLE | LBS_STANDARD,
		43,
		50,
		500,
		200,
		hWnd,
		(HMENU)ID_LIST,
		(HINSTANCE)GetWindowLongPtr(hWnd, GWLP_HINSTANCE),
		NULL
	);
	// 43, 280: create submit button (width: 500/585)
	hwndSubmit = CreateWindow(
		L"BUTTON",
		L"設定",
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
		43,
		280,
		240,
		50,
		hWnd,
		(HMENU)ID_SUBMIT,
		(HINSTANCE)GetWindowLongPtr(hWnd, GWLP_HINSTANCE),
		NULL
	);
	// 302, 280: create exit button (left: 585 - 43 - 240)
	hwndExit = CreateWindow(
		L"BUTTON",
		L"結束",
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
		302,
		280,
		240,
		50,
		hWnd,
		(HMENU)ID_EXIT,
		(HINSTANCE)GetWindowLongPtr(hWnd, GWLP_HINSTANCE),
		NULL
	);
}

// draw text (and load available versions), called by WM_PAINT
void Utils::DrawGUIText(HDC hdc)
{
	loadAvailable();

	std::wstring versionStr;
	std::wstring subverStr;
	// 43, 10: print version and subver
	if (this->loadVersion(L"data") && version != '\0') {
		versionStr = L"當前版本：　";
		subverStr = L"當前子版本：";
		// search from versions by version and subver
		auto it = std::find_if(versions.begin(), versions.end(), [copy=*this](const availableVersion& x) { return x.version == copy.version && x.subver == copy.subver; });

		selectedIdx = std::distance(versions.begin(), it);
		versionStr += (*it).strVersion;
		subverStr += (*it).strSubver;

		TextOut(hdc, 43, 10, versionStr.c_str(), versionStr.size());
		TextOut(hdc, 43, 30, subverStr.c_str(), subverStr.size());
	} else {
		versionStr = L"無法讀取當前版本";
		TextOut(hdc, 43, 10, versionStr.c_str(), versionStr.size());
	}

	// stop adding listbox for multiple rerender.
	if (loaded) return;

	std::wstring commonPrefix = L"D.R.LF2次世代(Special)";
	for (int i = 0; i < versions.size(); i++) {
		wchar_t itemNameBase[200];
		swprintf_s(itemNameBase, 200, L"%s %s", versions[i].strVersion.c_str(), versions[i].strSubver.c_str());
		// remove prefix for better experience
		wchar_t *itemName = &itemNameBase[commonPrefix.size()];
		int pos = (int)SendMessage(hwndListbox, LB_ADDSTRING, 0, (LPARAM)itemName);
	}
	loaded = true;
}

// receive selected version and save to variable
void Utils::SetSelectedIdx(int selected)
{
	selectedIdx = selected;
}

// perform copy and overwrite folder, then call redraw to update text
void Utils::submitCallback(HWND hWnd)
{
	SHFILEOPSTRUCT s = { 0 };
	s.hwnd = hWnd;
	s.wFunc = FO_COPY;
	s.fFlags = FOF_SILENT | FOF_NOCONFIRMATION;
	std::wstring src = versions[selectedIdx].strRealPath;
	std::wstring dst = std::wstring(L".");
	src += L'\0';
	dst += L'\0';
	s.pFrom = src.c_str();
	s.pTo = dst.c_str();
	SHFileOperation(&s);
	RedrawWindow(hWnd, NULL, NULL, RDW_INVALIDATE);
}

// just call WM_DESTROY to exit the program
void Utils::exitCallback(HWND hWnd)
{
	SendMessage(hWnd, WM_DESTROY, NULL, NULL);
}
