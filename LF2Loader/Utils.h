#pragma once
#include "framework.h"
#include "Resource.h"
#include <vector>

struct availableVersion {
	char version;
	char subver;
	std::wstring strVersion;
	std::wstring strSubver;
	std::wstring strRealPath;
};

class Utils
{
private:
	bool loaded = false;
	int selectedIdx = 0;
	char version = '\0';
	char subver = '\0';
	bool loadVersion(std::wstring path);
	bool loadAvailable();
	std::vector<struct availableVersion> versions;
public:
	HWND hwndSubmit;
	HWND hwndExit;
	HWND hwndListbox;
	void DrawGUIInterface(HWND hWnd);
	void DrawGUIText(HDC hdc);
	void SetSelectedIdx(int selected);
	void submitCallback(HWND hWnd);
	void exitCallback(HWND hWnd);
};

