LF2Loader
===

> This loader is only support d.r. lf2 because of the directory structure and file format.
> 此loader目前只支援d.r. lf2，歸功於該遊戲的資料夾結構與檔案特性。

Feature
---

1. Check current version and subversion of LF2.
2. Detect available versions in "版本選擇" folder.
3. Change the version by click.
4. Support for Windows XP and above. (x86 version)
 
1. 檢查當前的LF2版本與子版本
2. 偵測"版本選擇"資料夾內的可用版本
3. ㄧ鍵切換版本
4. 支援Windows XP(x86版本)

Usage
---

1. Put "LF2Loader.exe" into LF2 folder. (Check both data folder and "版本選擇" folder are exist)
2. Run "LF2Loader.exe".
3. Choose the version you want and click "設定".
4. Click "結束" and Run the game.

1. 將"LF2Loader.exe"放到LF2資料夾 (確保data資料夾與"版本選擇"資料夾都在)
2. 執行"LF2Loader.exe"
3. 選擇你要的版本後點擊"設定"
4. 點擊"結束"後執行遊戲

Compile Requirements
---

1. Visual Studio 2017
2. c++ windows xp support for vs 2017 (v141) tools (C++的Windows XP支援)

Screenshot
---

![Demo Screenshot](LF2Loader_Demo.PNG)